const prob4 = require('./problem4');

function carsOlderThan( inventory , givenYear ){
    const yearArr = prob4.getCarYears(inventory);
    let olderCar = 0;

    for( let i=0 ; i < yearArr.length ; i++ ){
        if( yearArr[i] < givenYear ){
            olderCar ++;
        }
    }
    
    if( olderCar > 0 ){
        console.log(`${ olderCar } cars were made before ${ givenYear }`);

        let carArr = [];

        for( let i = 0 ; i < inventory.length ; i++ ){
            if( inventory[i].car_year < givenYear ){
                carArr.push( inventory[i] );
            }
        }
        return carArr;
    }
    else {
        return `no cars older than ${ givenYear }`;
    }
}

module.exports = { carsOlderThan };