const findCarById = function( inventory,givenId ){
    try{
        let found=[];

        for(let i=0;i<inventory.length;i++){
            if(inventory[i].id === givenId){
                found.push(inventory[i]);
                break;
            }
        }
        return `Car ${givenId} is a ${found[0].car_year} ${found[[0]].car_make} ${found[0].car_model}`;
    }
    catch(e){
        return `no car found with id:${givenId}`;
    }
    
}

module.exports = {findCarById};