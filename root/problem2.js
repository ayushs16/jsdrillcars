const findLastCar = function(inventory){
    try {
        const ans = inventory[inventory.length - 1];

        return `Last car is ${ans.car_make} ${ans.car_model}`;
    }
    catch (error) {
        return `No cars found in inventory.`
    }
}

exports.lastCar = findLastCar;