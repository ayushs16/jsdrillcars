
const sortByModel = function( inventory , order = 1 ){
    const inventoryCopy = [ ... inventory];

    inventoryCopy.sort((a,b)=> {
        if(a.car_model.toLowerCase() >= b.car_model.toLowerCase() ){
            return 1 * order;
        }
        else{
            return -1 * order;
        }
    });
    return inventoryCopy;
}
module.exports = { sortByModel };