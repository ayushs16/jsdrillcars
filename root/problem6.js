const findByCarMake = function( inventory , carMake ){
    let arr = [ ... carMake ];
    let carsFound = [];

    for( let i = 0 ; i < inventory.length ; i++ ){
        if( arr.includes( inventory[i].car_make ) ){
            carsFound.push( inventory[i] );
        }
    }
    return carsFound;
}

module.exports = { findByCarMake };