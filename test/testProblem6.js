const { inventory } = require( '../app' );
const { findByCarMake } = require( '../root/problem6' );

// pass inventory and array of car_makes to find  as parameters.    
console.log( findByCarMake( inventory , [ "Audi" , "BMW" ] ) );